﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CameraFollow : MonoBehaviour
{
    public Transform follow;
    public Vector3 offset;
    public float linearSpeed = 0.1f;
    public float relativeSpeed = 1.0f;

    private void OnDestroy()
    {
    }

    void OnDrawGizmos()
    {
        UpdatePosition(1);
    }

	private void FixedUpdate ()
    {
        UpdatePosition(Time.deltaTime);
    }

    private void UpdatePosition(float dt)
    {
        if (follow && follow.gameObject.activeInHierarchy)
        {
            Vector3 position = transform.position;

            Vector3 targetPos = follow.position + offset;
            if (follow.rigidbody2D)
            {
                Vector2 vel = follow.rigidbody2D.velocity;
                targetPos += new Vector3(vel.x, vel.y, 0) / relativeSpeed * 1.2f;
            }

            position = Vector3.MoveTowards(position, targetPos, linearSpeed * dt);
            position = Vector3.Lerp(position, targetPos, relativeSpeed * dt);


            transform.position = position;
        }
    }

}
