﻿using UnityEngine;
using System.Collections;

public class Motor : MonoBehaviour
{
    public float forceFactor;
    public Vector2 forward = new Vector2(1, 0);

	void Start ()
    {
	}
	
	void FixedUpdate ()
    {
        float motor = Input.GetAxis("Vertical");

        forward.Normalize();

        Vector2 worldForward = transform.TransformDirection(forward);

        collider2D.attachedRigidbody.AddForceAtPosition(motor * worldForward * forceFactor, transform.position);

	}
}
