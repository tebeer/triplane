﻿using UnityEngine;
using System.Collections;

public class Steer : MonoBehaviour
{
    public float steeringAngle;

	void Start ()
    {
        m_startRotation = transform.localRotation;
	}
	
	void Update ()
    {
        float steer = Input.GetAxis("Horizontal");
        transform.localRotation = m_startRotation * Quaternion.Euler(0, 0, steeringAngle * steer);
	}

    private Quaternion m_startRotation;
}
