﻿using UnityEngine;
using System.Collections;

public class Wing : MonoBehaviour
{
    public float forceFactor;
    public Vector2 up = new Vector2(0, 1);

	void Start ()
    {
	}
	
	void FixedUpdate ()
    {
        up.Normalize();

        Vector2 worldUp = transform.TransformDirection(up);
        
        Vector2 velocity = collider2D.attachedRigidbody.velocity;

        float forwardFactor = -Vector2.Dot(velocity, worldUp);

        collider2D.attachedRigidbody.AddForceAtPosition(forwardFactor * worldUp * forceFactor, transform.position);

	}
}
